﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ComisionesElectronica.WebApi.Models;
using System.Web;
using System.Net.Http.Headers;
using System.Threading;
using System.Security.Cryptography;

namespace ComisionesElectronica.WebApi.Controllers
{
    public class MA_UsuariosController : ApiController
    {
        private ComisionesElectronicaEntities db = new ComisionesElectronicaEntities();
        private string userName;

        // GET: api/MA_Usuarios
        public IQueryable<MA_Usuarios> GetMA_Usuarios()
        {
            return db.MA_Usuarios;
        }
        // GET: api/Login?user={0}&pwd={1}
        [ResponseType(typeof(MA_Usuarios))]
        public async Task<IHttpActionResult> Login(string user, string pwd)
        {
            MA_Usuarios mA_Usuarios = await db.MA_Usuarios.Where(u => u.login_name == user && u.password == pwd).FirstOrDefaultAsync();
            if (mA_Usuarios == null)
            {
                return NotFound();
            }
            

            return Ok(mA_Usuarios);

        }
        // GET: api/MA_Usuarios/5
        [ResponseType(typeof(MA_Usuarios))]
        public async Task<IHttpActionResult> GetMA_Usuarios(string id)
        {
            MA_Usuarios mA_Usuarios = await db.MA_Usuarios.FindAsync(id);
            if (mA_Usuarios == null)
            {
                return NotFound();
            }

            return Ok(mA_Usuarios);
        }
        public HttpResponseMessage Get()
        {
            var resp = new HttpResponseMessage();
            var cookie = new CookieHeaderValue("session-Id", userName);
            cookie.Expires = DateTimeOffset.Now.AddDays(1);
            cookie.Domain = Request.RequestUri.Host;
            cookie.Path = "/";

            resp.Headers.AddCookies(new CookieHeaderValue[] { cookie });

            return resp;
        }
        // PUT: api/MA_Usuarios/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutMA_Usuarios(string id, MA_Usuarios mA_Usuarios)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != mA_Usuarios.codusuario)
            {
                return BadRequest();
            }

            db.Entry(mA_Usuarios).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MA_UsuariosExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/MA_Usuarios
        [ResponseType(typeof(MA_Usuarios))]
        public async Task<IHttpActionResult> PostMA_Usuarios(MA_Usuarios mA_Usuarios)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.MA_Usuarios.Add(mA_Usuarios);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (MA_UsuariosExists(mA_Usuarios.codusuario))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = mA_Usuarios.codusuario }, mA_Usuarios);
        }

        // DELETE: api/MA_Usuarios/5
        [ResponseType(typeof(MA_Usuarios))]
        public async Task<IHttpActionResult> DeleteMA_Usuarios(string id)
        {
            MA_Usuarios mA_Usuarios = await db.MA_Usuarios.FindAsync(id);
            if (mA_Usuarios == null)
            {
                return NotFound();
            }

            db.MA_Usuarios.Remove(mA_Usuarios);
            await db.SaveChangesAsync();

            return Ok(mA_Usuarios);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MA_UsuariosExists(string id)
        {
            return db.MA_Usuarios.Count(e => e.codusuario == id) > 0;
        }
    }
}