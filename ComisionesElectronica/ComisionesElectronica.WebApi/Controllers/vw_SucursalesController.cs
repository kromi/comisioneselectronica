﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ComisionesElectronica.WebApi.Models;

namespace ComisionesElectronica.WebApi.Controllers
{
    public class vw_SucursalesController : ApiController
    {
        private ComisionesElectronicaEntities db = new ComisionesElectronicaEntities();

        // GET: api/vw_Sucursales
        public IQueryable<vw_Sucursales> Getvw_Sucursales()
        {
            return db.vw_Sucursales;
        }

        // GET: api/vw_Sucursales/5
        [ResponseType(typeof(vw_Sucursales))]
        public async Task<IHttpActionResult> Getvw_Sucursales(string id)
        {
            vw_Sucursales vw_Sucursales = await db.vw_Sucursales.FindAsync(id);
            if (vw_Sucursales == null)
            {
                return NotFound();
            }

            return Ok(vw_Sucursales);
        }

        // PUT: api/vw_Sucursales/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> Putvw_Sucursales(string id, vw_Sucursales vw_Sucursales)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != vw_Sucursales.C_codigo)
            {
                return BadRequest();
            }

            db.Entry(vw_Sucursales).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!vw_SucursalesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/vw_Sucursales
        [ResponseType(typeof(vw_Sucursales))]
        public async Task<IHttpActionResult> Postvw_Sucursales(vw_Sucursales vw_Sucursales)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.vw_Sucursales.Add(vw_Sucursales);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (vw_SucursalesExists(vw_Sucursales.C_codigo))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = vw_Sucursales.C_codigo }, vw_Sucursales);
        }

        // DELETE: api/vw_Sucursales/5
        [ResponseType(typeof(vw_Sucursales))]
        public async Task<IHttpActionResult> Deletevw_Sucursales(string id)
        {
            vw_Sucursales vw_Sucursales = await db.vw_Sucursales.FindAsync(id);
            if (vw_Sucursales == null)
            {
                return NotFound();
            }

            db.vw_Sucursales.Remove(vw_Sucursales);
            await db.SaveChangesAsync();

            return Ok(vw_Sucursales);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool vw_SucursalesExists(string id)
        {
            return db.vw_Sucursales.Count(e => e.C_codigo == id) > 0;
        }
    }
}