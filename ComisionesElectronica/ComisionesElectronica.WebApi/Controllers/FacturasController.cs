﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ComisionesElectronica.WebApi.Models;

namespace ComisionesElectronica.WebApi.Controllers
{
    public class FacturasController : ApiController
    {
        private ComisionesElectronicaEntities db = new ComisionesElectronicaEntities();

        // GET: api/Facturas
        public IQueryable<Factura> GetFacturas()
        {
            return db.Facturas;
        }

        // GET: api/Facturas/5
        [ResponseType(typeof(Factura))]
        public async Task<IHttpActionResult> GetFactura(int id)
        {
            Factura factura = await db.Facturas.FindAsync(id);
            if (factura == null)
            {
                return NotFound();
            }

            return Ok(factura);
        }

        // POST: api/Facturas
        [ResponseType(typeof(Factura))]
        public async Task<IHttpActionResult> PostFactura()
        {
            if (!ModelState.IsValid)
            {
                return await Task<IHttpActionResult>.FromResult(BadRequest(ModelState));
            }

            var transacciones = db.v_TransaccionesSucursales.ToList();

            try
            {
                db.cap_ActualizarFacturasElectronica();
            }
            catch(Exception ex)
            {
                return await Task<IHttpActionResult>.FromResult(InternalServerError(ex));
            }

            return await Task<IHttpActionResult>.FromResult(Ok());
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FacturaExists(string numeroFactura)
        {
            return db.Facturas.Count(e => e.NumeroFactura == numeroFactura) > 0;
        }
    }
}