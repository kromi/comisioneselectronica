﻿using ComisionesElectronica.WebApi.Models;
using ComisionesElectronica.WebApi.Models.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Http;

namespace ComisionesElectronica.WebApi.Controllers
{
    public class AuthenticationController : ApiController
    {
        private ComisionesElectronicaEntities db = new ComisionesElectronicaEntities();
        [Route("authenticate")]
        public IHttpActionResult Authenticate(AuthenticateViewModel viewModel)
        {
            
            if (!db.MA_Usuarios.Any(u => u.login_name == viewModel.Username && u.password == viewModel.Password))
            {
                return Ok(new { success = false, message = "Nombre de Usuario o contraseña no válidos" });
            }
            return Ok(new { success = true });
        }

        private static void SetPrincipal(IPrincipal principal)
        {
            Thread.CurrentPrincipal = principal;
            if (HttpContext.Current != null)
            {
                HttpContext.Current.User = principal;
            }
        }
    }
}
