﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ComisionesElectronica.WebApi.Models;
using System.Web;
using System.Net.Http.Headers;

namespace ComisionesElectronica.WebApi.Controllers
{
    public class ComisionElectronicasController : ApiController
    {
        private ComisionesElectronicaEntities db = new ComisionesElectronicaEntities();

        // GET: api/ComisionElectronicas
        public IQueryable<ComisionElectronica> GetComisionElectronicas()
        {
            return db.ComisionElectronicas;
        }

        // GET: api/ComisionElectronicas/5
        [ResponseType(typeof(ComisionElectronica))]
        public async Task<IHttpActionResult> GetComisionElectronica(int id)
        {
            ComisionElectronica comisionElectronica = await db.ComisionElectronicas.FindAsync(id);
            if (comisionElectronica == null)
            {
                return NotFound();
            }

            return Ok(comisionElectronica);
        }

        // PUT: api/ComisionElectronicas/5
        [ResponseType(typeof(void))]
        [Authorize(Users = "CCANDELORI")]
        public async Task<IHttpActionResult> PutComisionElectronica(int id, ComisionElectronica comisionElectronica)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != comisionElectronica.ComisionId)
            {
                return BadRequest();
            }

            db.Entry(comisionElectronica).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!db.ComisionElectronicas.Any(c => c.ComisionId == id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ComisionElectronicas?numeroFactura={0}&codVendedor={1}
        [ResponseType(typeof(ComisionElectronica))]
        [HttpPost]
        public async Task<IHttpActionResult> PostComisionElectronica(string numeroFactura, string codVendedor)
        {
            if (!ModelState.IsValid)
            {
                return await Task<IHttpActionResult>.FromResult(BadRequest(ModelState));
            }

            if (ComisionElectronicaExists(numeroFactura))
            {
                return await Task<IHttpActionResult>.FromResult(BadRequest("Comisión a Factura ya asignada"));
            }
            List<Factura> facturas = db.Facturas.Where(f => f.NumeroFactura == numeroFactura && f.C_Concepto == "VEN").ToList();
            vw_PorcentajeComision porcentaje = db.vw_PorcentajeComision.Where(p => p.SettingKey == "ComisionElectronica").FirstOrDefault();
            vw_Vendedores vendedor = null;
            if (db.vw_Vendedores.Any(v => v.Codigo == codVendedor))
                vendedor = db.vw_Vendedores.Where(v => v.Codigo == codVendedor).FirstOrDefault();
            else
                return await Task<IHttpActionResult>.FromResult(BadRequest("Vendedor no existe"));
            CookieHeaderValue cookie = Request.Headers.GetCookies("session-Id").FirstOrDefault();
            if (cookie != null)
            {

            }

            foreach (var factura in facturas)
            {
                ComisionElectronica comision = new ComisionElectronica();
                comision.C_NumeroFactura = factura.NumeroFactura;
                comision.Cod_Vendedor = codVendedor;
                comision.NombreVendedor = vendedor.Nombre;
                comision.Porcentaje_Comision = Convert.ToDecimal(porcentaje.Value);
                comision.SubTotal = factura.SubTotal.Value;
                comision.Sucursal = factura.Sucursal;
                comision.Cod_Producto = factura.Cod_Principal;
                comision.Concepto = factura.C_Concepto;
                comision.FechaFactura = factura.FechaFactura;
                comision.CreadoPor = User.Identity.Name;
                comision.CreadoEl = DateTime.Now;
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        db.ComisionElectronicas.Add(comision);
                        db.SaveChanges();
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        return await Task<IHttpActionResult>.FromResult(InternalServerError(ex));
                    }
                }
            }

            return Ok("Última Factura asignada a " + vendedor.Nombre);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ComisionElectronicaExists(string numeroFactura)
        {
            return db.ComisionElectronicas.Count(e => e.C_NumeroFactura == numeroFactura) > 0;
        }
    }
}