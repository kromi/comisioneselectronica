﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ComisionesElectronica.WebApi.Controllers
{
    [Authorize]
    public class SecureDataController : ApiController
    {
        public IHttpActionResult Get()
        {
            return Ok(new { secureData = "Has sido auntenticado para acceder aqui" });
        }
    }
}
