﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ComisionesElectronica.WebApi.Models;

namespace ComisionesElectronica.WebApi.Controllers
{
    public class v_TransaccionesSucursalesController : ApiController
    {
        private ComisionesElectronicaEntities db = new ComisionesElectronicaEntities();

        // GET: api/v_TransaccionesSucursales
        public IQueryable<v_TransaccionesSucursales> Getv_TransaccionesSucursales()
        {
            return db.v_TransaccionesSucursales;
        }

        // GET: api/v_TransaccionesSucursales/5
        [ResponseType(typeof(v_TransaccionesSucursales))]
        public async Task<IHttpActionResult> Getv_TransaccionesSucursales(string id)
        {
            v_TransaccionesSucursales v_TransaccionesSucursales = await db.v_TransaccionesSucursales.FindAsync(id);
            if (v_TransaccionesSucursales == null)
            {
                return NotFound();
            }

            return Ok(v_TransaccionesSucursales);
        }

        // PUT: api/v_TransaccionesSucursales/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> Putv_TransaccionesSucursales(string id, v_TransaccionesSucursales v_TransaccionesSucursales)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != v_TransaccionesSucursales.C_Numero)
            {
                return BadRequest();
            }

            db.Entry(v_TransaccionesSucursales).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!v_TransaccionesSucursalesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/v_TransaccionesSucursales
        [ResponseType(typeof(v_TransaccionesSucursales))]
        public async Task<IHttpActionResult> Postv_TransaccionesSucursales(v_TransaccionesSucursales v_TransaccionesSucursales)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.v_TransaccionesSucursales.Add(v_TransaccionesSucursales);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (v_TransaccionesSucursalesExists(v_TransaccionesSucursales.C_Numero))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = v_TransaccionesSucursales.C_Numero }, v_TransaccionesSucursales);
        }

        // DELETE: api/v_TransaccionesSucursales/5
        [ResponseType(typeof(v_TransaccionesSucursales))]
        public async Task<IHttpActionResult> Deletev_TransaccionesSucursales(string id)
        {
            v_TransaccionesSucursales v_TransaccionesSucursales = await db.v_TransaccionesSucursales.FindAsync(id);
            if (v_TransaccionesSucursales == null)
            {
                return NotFound();
            }

            db.v_TransaccionesSucursales.Remove(v_TransaccionesSucursales);
            await db.SaveChangesAsync();

            return Ok(v_TransaccionesSucursales);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool v_TransaccionesSucursalesExists(string id)
        {
            return db.v_TransaccionesSucursales.Count(e => e.C_Numero == id) > 0;
        }
    }
}