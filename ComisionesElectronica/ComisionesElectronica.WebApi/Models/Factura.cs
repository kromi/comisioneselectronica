//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ComisionesElectronica.WebApi.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Factura
    {
        public int Id { get; set; }
        public string NumeroFactura { get; set; }
        public string Cod_Principal { get; set; }
        public string C_Concepto { get; set; }
        public string Sucursal { get; set; }
        public Nullable<double> SubTotal { get; set; }
        public Nullable<System.DateTime> FechaFactura { get; set; }
    }
}
