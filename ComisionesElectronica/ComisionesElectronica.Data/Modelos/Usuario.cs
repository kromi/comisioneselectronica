﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComisionesElectronica.Data.Modelos
{
    public class Usuario
    {
        [Key]
        [Column("Row")]
        public int Row { get; set; }
        [Column("codusuario")]
        public string CodUsuario { get; set; }
        [Column("password")]
        public string Password { get; set; }
        [Column("login_name")]
        public string Login { get; set; }
        [Column("descripcion")]
        public string Descripcion { get; set; }
        [Column("clave")]
        public string Clave { get; set; }
        [Column("localidad")]
        public string Localidad { get; set; }
        [Column("Nivel")]
        public decimal Nivel { get; set; }
        [Column("Vendedor")]
        public decimal Vendedor { get; set; }
        [Column("add_date")]
        public DateTime Add_date { get; set; }
        [Column("update_date")]
        public DateTime Update_date { get; set; }
        [Column("tipo_usuario")]
        public bool Tipo_Usuario { get; set; }
        [Column("ID")]
        public decimal Id { get; set; }
        [Column("BS_ACTIVO")]
        public bool Bs_Activo { get; set; }
    }
}
