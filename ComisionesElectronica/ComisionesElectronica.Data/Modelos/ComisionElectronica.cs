﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComisionesElectronica.Data.Modelos
{
    [Table("ComisionElectronica")]
    public class ComisionElectronica
    {
        [Key]
        public int ComisionId { get; set; }
        [Column("C_NumeroFactura")]
        public string NumeroFactura { get; set; }
        public string Cod_Vendedor { get; set; }
        public string NombreVendedor { get; set; }
        public string Cod_Producto { get; set; }
        public string Concepto { get; set; }
        public double SubTotal { get; set; }
        public decimal Porcentaje_Comision { get; set; }
        public string Sucursal { get; set; }
        public string CreadoPor { get; set; }
        public DateTime CreadoEl { get; set; }
        [Column("FechaFactura")]
        public DateTime Fecha { get; set; }


    }
}
