﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComisionesElectronica.Data.Modelos
{
    public class Factura
    {
        [Key]
        [Column("Id")]
        public int Id { get; set; }
        [Column("NumeroFactura")]
        public string NumeroFactura { get; set; }
        [Column("Cod_Principal")]
        public string Cod_Principal { get; set; }
        [Column("C_Concepto")]
        public string Concepto { get; set; }
        [Column("Sucursal")]
        public string Sucursal { get; set; }
        [Column("Subtotal")]
        public double SubTotal { get; set; }
        [Column("FechaFactura")]
        public DateTime Fecha { get; set; }
    }
}
