﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComisionesElectronica.Data.Modelos
{
    public class PorcentajeComision
    {
        [Key]
        public int Row { get; set; }
        public string SettingKey { get; set; }
        public string Value { get; set; }
    }
}
