﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComisionesElectronica.Data.Modelos
{
    public class Sucursal
    {
        [Key]
        [Column("Row")]
        public int Row { get; set; }
        [Column("C_codigo")]
        public string C_codigo { get; set; }
        [Column("C_descripcion")]
        public string C_descripcion { get; set; }
    }
}
