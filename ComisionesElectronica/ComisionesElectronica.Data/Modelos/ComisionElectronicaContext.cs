﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComisionesElectronica.Data.Modelos
{
    public class ComisionElectronicaContext : DbContext
    {
        public ComisionElectronicaContext():base("ComisionesElectronica")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            Database.SetInitializer<ComisionElectronicaContext>(null);
            modelBuilder.Entity<Usuario>()
                .ToTable("MA_Usuarios")
                .HasKey(u => u.Row);
            modelBuilder.Entity<Vendedor>()
                .ToTable("vw_Vendedores")
                .HasKey(v => v.Row);
            modelBuilder.Entity<Sucursal>()
                .ToTable("vw_Sucursales")
                .HasKey(s => s.Row);
            modelBuilder.Entity<Factura>()
                .ToTable("Factura")
                .HasKey(f => f.Id);
            modelBuilder.Entity<PorcentajeComision>()
                .ToTable("vw_PorcentajeComision")
                .HasKey(p => p.Row);
        }

        public DbSet<ComisionElectronica> ComisionesElectronica { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Vendedor> Vendedores { get; set; }
        public DbSet<Sucursal> Sucursales { get; set; }
        public DbSet<Factura> Facturas { get; set; }
        public DbSet<PorcentajeComision> PorcentajesComision { get; set; }
    }
}
