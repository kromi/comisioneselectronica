﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComisionesElectronica.Data.Modelos
{
    public class Vendedor
    {
        [Key]
        public int Row { get; set; }
        [Column("Codigo")]
        public string Codigo { get; set; }
        [Column("Nombre")]
        public string Nombre { get; set; }
    }
}
