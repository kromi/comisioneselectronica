﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ComisionesWeb
{
    public partial class Login : System.Web.UI.Page
    {
        private ComisionesElectronicaEntities db = new ComisionesElectronicaEntities();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Login1_Authenticate(object sender, AuthenticateEventArgs e)
        {
            if (db.MA_Usuarios.Any(u => u.login_name == Login1.UserName && u.password == Login1.Password))
            {
                var identity = new GenericIdentity(Login1.UserName);
                SetPrincipal(new GenericPrincipal(identity, null));
                e.Authenticated = true;
                FormsAuthentication.RedirectFromLoginPage(Login1.UserName, false);
                Response.Redirect("~/Ubicacion.aspx");
            }
            else
                e.Authenticated = false;

        }

        private static void SetPrincipal(IPrincipal principal)
        {
            Thread.CurrentPrincipal = principal;
            if (HttpContext.Current != null)
            {
                HttpContext.Current.User = principal;
            }
        }
    }
}