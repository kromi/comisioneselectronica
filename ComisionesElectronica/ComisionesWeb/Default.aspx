﻿<%@ Page Title="Principal" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ComisionesWeb._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        function ShowMessage(message) {
            alert(message);
        }
    </script>
    <asp:Panel ID="Panel1" runat="server">
        <table style="width:100%;">
            <tr>
                <td>&nbsp;</td>
                <td style="text-align:center;font-weight:bold;">Asignar comisión</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <table style="width:100%;">
                        <tr>
                        <td style="text-align:left;" colspan="3">
                            <asp:Label ID="m_nombreUsuario" runat="server" Text=""></asp:Label>
                        </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="3" style="text-align:left;">
                                <asp:Label ID="m_sucursal" runat="server" Text=""></asp:Label>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align:right">
                                <asp:Label ID="Label1" runat="server" Text="Facturas Sin Asignar:" style="font-size: large"></asp:Label>
                            </td>
                            <td style="text-align:right">&nbsp;</td>
                            <td style="text-align:left">
                                <asp:DropDownList ID="m_facturasList" runat="server" AppendDataBoundItems="true" style="font-size: large">
                                    <asp:ListItem Text="[SELECCIONE UNA FACTURA]" Value=""></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: right">&nbsp;</td>
                            <td style="text-align: right">&nbsp;</td>
                            <td style="text-align: left">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align:right">
                                <asp:Label ID="Label2" runat="server" Text="Vendedores:" style="font-size: large"></asp:Label>
                            </td>
                            <td style="text-align:right">&nbsp;</td>
                            <td style="text-align:left">
                                <asp:DropDownList ID="m_vendedoresList" runat="server" AppendDataBoundItems="true" style="font-size: large">
                                    <asp:ListItem Text="[SELECCIONE UN(A) VENDEDOR(A)]" Value=""></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align:left" colspan="3">
                                <asp:Label ID="m_errorLabel" runat="server" Font-Bold="true" Font-Size="Medium" Text=""></asp:Label>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td style="text-align:left">
                                <asp:Button ID="m_continuar" CssClass="btn-success" runat="server" Text="Guardar" OnClick="m_continuar_Click" Height="35px" Width="105px" style="font-size: large" />
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </asp:Panel>

</asp:Content>
