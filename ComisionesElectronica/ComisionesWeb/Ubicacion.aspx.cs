﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ComisionesWeb
{
    public partial class Ubicacion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                using (var db = new ComisionesElectronicaEntities())
                {
                    m_nombreUsuario.Text = "Usuario: " + User.Identity.Name;
                    m_sucursales.DataSource = db.vw_Sucursales.Select(v => new { v.C_descripcion, v.C_codigo }).ToList();
                    m_sucursales.DataTextField = "C_descripcion";
                    m_sucursales.DataValueField = "C_codigo";

                    m_sucursales.DataBind();
                }
            }
        }

        protected void m_continuar_Click(object sender, EventArgs e)
        {
            if (m_sucursales.SelectedValue == "")
                m_error.Text = "Debe seleccionar una sucursal";
            ConfigurationManager.AppSettings["cod_sucursal"] = m_sucursales.SelectedValue;
            ConfigurationManager.AppSettings["desc_sucursal"] = m_sucursales.SelectedItem.Text;
            Session.Timeout = 560;
            Response.Redirect("~/");
        }
    }
}