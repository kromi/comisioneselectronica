﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Ubicacion.aspx.cs" Inherits="ComisionesWeb.Ubicacion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <table style="width:100%;">
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <table style="width:100%;">
                    <tr>
                        <td style="text-align:left;">
                            <asp:Label ID="m_nombreUsuario" runat="server" Text=""></asp:Label>
                        </td>
                        <td>&nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:left">
                            <asp:Label ID="Label1" runat="server" Text="Seleccione una sucursal"></asp:Label>
                        </td>
                        <td style="text-align:left">
                            <asp:DropDownList ID="m_sucursales" runat="server" AppendDataBoundItems="True">
                                <asp:ListItem Text="[SELECCIONE UNA SUCURSAL]" Value=""></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td style="text-align:left">
                            <asp:Button ID="m_continuar" CssClass="btn btn-success" runat="server" Text="Guardar" OnClick="m_continuar_Click" Height="35px" Width="105px" /></td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:Label ID="m_error" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
            </td>
            <td>&nbsp;</td>
        </tr>
    </table>

</asp:Content>
