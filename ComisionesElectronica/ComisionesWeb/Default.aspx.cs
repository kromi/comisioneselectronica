﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ComisionesWeb
{
    public partial class _Default : Page
    {
        private ComisionesElectronicaEntities db = new ComisionesElectronicaEntities();


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["cod_sucursal"]))
                    Response.Redirect("~/Ubicacion");

                m_nombreUsuario.Text = $"{"Usuario: "}{User.Identity.Name}";
                m_sucursal.Text = $"{"Sucursal: "}{ConfigurationManager.AppSettings["desc_sucursal"]}";
                string codigoSucursal = ConfigurationManager.AppSettings["cod_sucursal"];
                var facturasConComision = db.ComisionElectronicas.Select(c => c.C_NumeroFactura);
                List<Factura> facturas = new List<Factura>();
                var facturasSinComision = db.Facturas.Where(f => f.Sucursal == codigoSucursal).Distinct().Select(x => x.NumeroFactura).Except(facturasConComision).ToList();
                foreach (var factura in facturasSinComision)
                {
                    facturas.Add(new Factura() { NumeroFactura = factura });
                }

                m_facturasList.DataSource = facturas;
                m_facturasList.DataTextField = "NumeroFactura";
                m_facturasList.DataValueField = "NumeroFactura";
                m_facturasList.DataBind();

                m_vendedoresList.DataSource = db.vw_Vendedores.Select(v => new { v.Codigo, v.Nombre }).ToList();
                m_vendedoresList.DataTextField = "Nombre";
                m_vendedoresList.DataValueField = "Codigo";
                m_vendedoresList.DataBind();
            }
        }

        protected void m_continuar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["cod_sucursal"]))
                Response.Redirect("~/Ubicacion");

            if (m_facturasList.SelectedValue != null && m_vendedoresList.SelectedValue != null)
            {
                string codVendedor = "";
                string codigoSucursal = ConfigurationManager.AppSettings["cod_sucursal"];
                if (db.ComisionElectronicas.Any(f => f.C_NumeroFactura == m_facturasList.SelectedValue.ToString()))
                {
                    m_errorLabel.Text = "Factura ya asignada, por favor actualice el listado de facturas";
                    m_errorLabel.ForeColor = Color.Red;
                    return;
                }
                var facturas = (from f in db.Facturas
                    where f.NumeroFactura == m_facturasList.SelectedValue.ToString() && f.C_Concepto == "VEN"
                    group f by new { f.Cod_Principal, f.C_Concepto, f.FechaFactura, f.NumeroFactura, f.Sucursal } into g
                    select new {
                        Cod_Principal = g.Key.Cod_Principal,
                        C_Concepto = g.Key.C_Concepto,
                        FechaFactura = g.Key.FechaFactura,
                        NumeroFactura = g.Key.NumeroFactura,
                        Sucursal = g.Key.Sucursal,
                        SubTotal = g.Sum(n => n.SubTotal)

                    }).ToArray();
                vw_PorcentajeComision porcentaje = db.vw_PorcentajeComision.Where(p => p.SettingKey == "ComisionElectronica").FirstOrDefault();
                decimal pctComision = Convert.ToDecimal(porcentaje.Value, CultureInfo.GetCultureInfo("en"));
                codVendedor = m_vendedoresList.SelectedValue.ToString();
                vw_Vendedores vendedor = db.vw_Vendedores.Where(v => v.Codigo == codVendedor).FirstOrDefault();
                string nombreVendedor = vendedor.Nombre;
                foreach (var factura in facturas)
                {
                    ComisionElectronica comision = new ComisionElectronica();
                    comision.C_NumeroFactura = factura.NumeroFactura;
                    comision.Cod_Vendedor = codVendedor;
                    comision.NombreVendedor = nombreVendedor.Substring(4, nombreVendedor.Length - 4);
                    comision.Porcentaje_Comision = pctComision;
                    comision.SubTotal = factura.SubTotal.Value;
                    comision.Sucursal = codigoSucursal;
                    comision.Cod_Producto = factura.Cod_Principal;
                    comision.Concepto = factura.C_Concepto;
                    comision.FechaFactura = factura.FechaFactura;
                    comision.CreadoPor = User.Identity.Name;
                    comision.CreadoEl = DateTime.Now;
                    using (var transaction = db.Database.BeginTransaction())
                    {
                        try
                        {
                            db.ComisionElectronicas.Add(comision);
                            db.SaveChanges();
                            transaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            m_errorLabel.Text = ex.InnerException.Message;
                            m_errorLabel.ForeColor = Color.Red;
                            transaction.Rollback();

                        }
                        Show($"{"Factura No."}{comision.C_NumeroFactura}{" asignada a "}{comision.NombreVendedor}");
                        //m_errorLabel.Text = $"{"Última Factura asignada a "}{vendedor.Nombre}";
                        //m_errorLabel.ForeColor = Color.Green;
                        //ClientScript.RegisterClientScriptBlock(this.GetType(), "Confirmación!", $"{"<script language='javascript'>alert('Factura No."}{comision.C_NumeroFactura}{" asignada a "}{comision.NombreVendedor}{"');</script>"}",true);
                    }
                }

                Response.Redirect("~/");
            }
            else
            {
                m_errorLabel.Text = "Debe Seleccionar una Factura y un Vendedor, gracias";
            }

        }

        public static void Show(string message)
        {
            string cleanMessage = message.Replace("'", "\'");
            Page page = HttpContext.Current.CurrentHandler as Page;
            string script = string.Format("alert('{0}');", cleanMessage);
            if (page != null && !page.ClientScript.IsClientScriptBlockRegistered("alert"))
            {
                page.ClientScript.RegisterOnSubmitStatement(page.GetType(), "alert", $"{"<script type='text/javascript'>"}{script}{"</script>"}");
            }
        }
    }
}