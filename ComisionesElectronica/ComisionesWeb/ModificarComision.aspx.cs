﻿using ComisionesWeb;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ComisionesWeb
{
    public partial class ModificarComision : System.Web.UI.Page
    {
        private ComisionesElectronicaEntities db = new ComisionesElectronicaEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["cod_sucursal"]))
            //    Response.Redirect("~/Ubicacion");
            m_vendedoresList.DataSource = db.vw_Vendedores.Select(v => new { v.Codigo, v.Nombre }).ToList();
            m_vendedoresList.DataTextField = "Nombre";
            m_vendedoresList.DataValueField = "Codigo";
            m_vendedoresList.DataBind();
        }

        protected void m_submit_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(m_factura.Text) && m_vendedoresList.SelectedValue != null)
            {
                string codvendedor = m_vendedoresList.SelectedValue.ToString();
                using (var transaction = db.Database.BeginTransaction())
                {

                    vw_Vendedores vendedor = db.vw_Vendedores.Where(x => x.Codigo == codvendedor).FirstOrDefault();
                    ComisionElectronica comision = db.ComisionElectronicas.Where(c => c.C_NumeroFactura == m_factura.Text).FirstOrDefault();
                    try
                    {
                        comision.Cod_Vendedor = codvendedor;
                        comision.NombreVendedor = vendedor.Nombre;
                        db.SaveChanges();
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        m_errorLabel.Text = ex.InnerException.Message;
                        m_errorLabel.ForeColor = Color.Red;
                        transaction.Rollback();
                        return;
                    }
                    m_errorLabel.Text = "Comision reasignada a " + vendedor.Nombre;
                    m_errorLabel.ForeColor = Color.Green;
                }
            }
        }
    }
}