﻿<%@ Page Title="Modificar Comisión" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ModificarComision.aspx.cs" Inherits="ComisionesWeb.ModificarComision" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <table style="width: 100%;">
        <tr>
            <td>&nbsp;</td>
            <td style="text-align:center;font-weight:bold;">Modificar comisión asignada</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <table style="width:100%;">
                    <tr>
                        <td style="text-align:left;">
                            <asp:Label ID="m_nombreUsuario" runat="server" Text=""></asp:Label>
                        </td>
                        <td style="text-align:left;">
                            &nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:left;">
                            <asp:Label ID="m_sucursal" runat="server" Text=""></asp:Label>
                        </td>
                        <td style="text-align:left;">
                            &nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <asp:Label ID="Label1" runat="server" Text="Número de Factura:"></asp:Label>
                        </td>
                        <td>&nbsp;</td>
                        <td style="text-align:left">
                            <asp:TextBox ID="m_factura" runat="server"></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <asp:Label ID="Label2" runat="server" Text="Vendedor:"></asp:Label>
                        </td>
                        <td>&nbsp;</td>
                        <td style="text-align:left">
                            <asp:DropDownList ID="m_vendedoresList" runat="server" AppendDataBoundItems="true" style="font-size: large" TabIndex="1">
                                    <asp:ListItem Text="[SELECCIONE UN(A) VENDEDOR(A)]" Value=""></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td colspan="3">
                                <asp:Label ID="m_errorLabel" runat="server" Font-Bold="true" Font-Size="Medium" Text=""></asp:Label>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <asp:Button ID="m_submit" OnClick="m_submit_Click" CssClass="btn btn-success" runat="server" Text="Guardar" TabIndex="2" />
                        </td>
                        <td>&nbsp;</td>
                        <td style="text-align:left">
                            <asp:Button ID="m_goBack" CssClass="btn btn-danger" runat="server" PostBackUrl="~/Principal.aspx" Text="Regresar" TabIndex="3" />
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
</asp:Content>
