﻿<%@ Page Title="Inicio de Sesión" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="ComisionesWeb.Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <table style="width: 100%;">
        <tr>
            <td style="width: 30px">&nbsp;</td>
            <td style="width:700px;">
                &nbsp;</td>
            <td>    
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 30px">&nbsp;</td>
            <td>    
                <asp:Login ID="Login1" runat="server" FailureText="Intento de Inicio de Sesión Fallido" LoginButtonText="Iniciar Sesión" PasswordLabelText="Contraseña:" PasswordRequiredErrorMessage="Contraseña es requerida." RememberMeText="Recordarme" TitleText="Inicio de Sesión" UserNameLabelText="Usuario:" UserNameRequiredErrorMessage="Usuario es requerido" BackColor="#F7F6F3" BorderColor="#E6E2D8" BorderPadding="4" BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" Font-Size="0.8em" ForeColor="#333333" OnAuthenticate="Login1_Authenticate" DisplayRememberMe="False" RememberMeSet="True" Height="300px" Width="400px">
                    <InstructionTextStyle Font-Italic="True" ForeColor="Black" />
                    <LayoutTemplate>
                        <table style="border-collapse:collapse;">
                            <tr>
                                <td>
                                    <table style="height:300px;width:400px;">
                                        <tr>
                                            <td colspan="2" style="text-align:center;color:White;background-color:#5D7B9D;font-size:0.9em;font-weight:bold;">Inicio de Sesión</td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:center;">
                                                <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">Usuario:</asp:Label>
                                            </td>
                                            <td style="text-align:left;">
                                                <asp:TextBox ID="UserName" runat="server" Font-Size="0.8em"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" ErrorMessage="Usuario es requerido" ToolTip="Usuario es requerido" ValidationGroup="Login1">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:center;">
                                                <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Contraseña:</asp:Label>
                                            </td>
                                            <td style="text-align:left;">
                                                <asp:TextBox ID="Password" runat="server" Font-Size="0.8em" TextMode="Password"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password" ErrorMessage="Contraseña es requerida." ToolTip="Contraseña es requerida." ValidationGroup="Login1">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:center;" colspan="2" style="color:Red;">
                                                <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:center;" colspan="2">
                                                <asp:Button ID="LoginButton" runat="server" CommandName="Login" CssClass="btn btn-primary" Text="Iniciar Sesión" ValidationGroup="Login1" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <LoginButtonStyle BackColor="#FFFBFF" BorderColor="#CCCCCC" BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" Font-Size="0.8em" ForeColor="#284775" />
                    <TextBoxStyle Font-Size="0.8em" />
                    <TitleTextStyle BackColor="#5D7B9D" Font-Bold="True" Font-Size="0.9em" ForeColor="White" />

                </asp:Login>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 30px">    
                &nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
</asp:Content>
