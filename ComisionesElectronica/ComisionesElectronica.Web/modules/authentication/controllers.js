﻿'use strict';

angular.module('Authentication')

.controller('LoginController',
    ['$scope', '$rootScope', '$location', 'AuthenticationService',
    function ($scope, $rootScope, $location, AuthenticationService) {
        // reset login status
        AuthenticationService.ClearCredentials();

        $scope.login = function () {
            $scope.dataLoading = true;
            AuthenticationService.Login($scope.username, $scope.password, function (response) {
                $scope.temp = angular.fromJson(response);
                if ($scope.temp.success == true) {
                    AuthenticationService.SetCredentials($scope.username, $scope.password);
                    $location.path('/ubicacion');
                }
                else {
                    $scope.error = $scope.temp.message
                    $scope.dataLoading = false;
                }
            });
        };
    }]);