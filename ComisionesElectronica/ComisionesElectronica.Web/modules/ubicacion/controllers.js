﻿'use strict';

angular.module('Ubicacion')

.controller('UbicacionController',
    ['$scope', '$rootScope', '$location', 'UbicacionService',
        function ($scope, $rootScope, $location, UbicacionService) {
            UbicacionService.GetSecureData(function (response) {
                $scope.temp = angular.fromJson(response);
                $scope.secureData = $scope.temp.secureData;
            });
        }
        
    ]);