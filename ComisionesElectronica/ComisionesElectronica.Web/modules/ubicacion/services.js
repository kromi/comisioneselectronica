﻿'use strict';

angular.module('Ubicacion')

.factory('UbicacionService',
    ['$http',
    function ($http) {
        var service = {};

        service.GetSecureData = function (callback) {
            $http.get('../ComisionesElectronica.WebApi/api/securedata')
                .then(function (response) {
                    var data = response.data;
                    callback(data);
                });
        };

        return service;
    }]);