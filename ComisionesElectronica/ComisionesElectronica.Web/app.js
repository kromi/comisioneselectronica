﻿/// <reference path="C:\Users\Manuel\Source\Repos\comisioneselectronica\ComisionesElectronica\ComisionesElectronica.Web\Scripts/angular.js" />
'use strict';

// declare modules
angular.module('Authentication', []);
angular.module('Ubicacion', []);
angular.module('Home', []);
angular.module('Modificar', []);

angular.module('MyApp', [
    'Authentication',
    'Ubicacion',
    'Home',
    'Modificar',
    'ngRoute',
    'ngCookies'
]).
config(
    function ($routeProvider) {
        $routeProvider
            .when('/login', {
                templateUrl: 'modules/authentication/views/login.html',
                controller: 'LoginController'
            })
            .when('/ubicacion', {
                templateUrl: 'modules/ubicacion/views/selectLocation.html',
                controller: 'UbicacionController'
            })
            .when('/home', {
                templateUrl: 'modules/home/views/home.html',
                controller: 'HomeController'
            })
            .when('/modificar', {
                templateUrl: 'modules/modificar/views/edit.html',
                controller: 'ModificarController'
            })
            .otherwise({  redirectTo: '/home' });
    }
)
    .run(['$rootScope', '$location', '$cookieStore', '$http',
    function ($rootScope, $location, $cookieStore, $http) {
        // keep user logged in after page refresh
        $rootScope.globals = $cookieStore.get('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
        }

        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            // redirect to login page if not logged in
            if ($location.path() !== '/login' && !$rootScope.globals.currentUser) {
                $location.path('/login');
            }
        });
    }]);
