﻿using ComisionesElectronica.Data.Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ComisionesElectronica.Windows
{
    public partial class ReasignarComision : Form
    {
        private ComisionElectronicaContext db;
        public ReasignarComision()
        {
            InitializeComponent();
            db = new ComisionElectronicaContext();
        }

        private void ActualizarVendedores()
        {         

            m_vendedoresList.DataSource = db.Vendedores.ToList();
            m_vendedoresList.DisplayMember = "Nombre";
            m_vendedoresList.ValueMember = "Codigo";
        }

        private void m_facturaText_Leave(object sender, EventArgs e)
        {
            bool ExisteFactura = db.ComisionesElectronica.Any(f => f.NumeroFactura == m_facturaText.Text);
            if (ExisteFactura)
            {
                m_errorLabel.Text = "Si existe factura puede continuar";
                m_errorLabel.ForeColor = Color.White;
            }
            else
            {
                m_errorLabel.Text = "El número de factura descrito no existe o no tiene asignada alguna comisión";
                m_errorLabel.ForeColor = Color.Red;
            }
        }

        private void m_regresarLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Close();
        }

        private void m_guardarButton_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(m_facturaText.Text) && m_vendedoresList.SelectedValue != null)
            {
                string codvendedor = m_vendedoresList.SelectedValue.ToString();
                using (var transaction = db.Database.BeginTransaction())
                {

                    Vendedor vendedor = db.Vendedores.Where(x => x.Codigo == codvendedor).FirstOrDefault();
                    ComisionElectronica comision = db.ComisionesElectronica.Where(c => c.NumeroFactura == m_facturaText.Text).FirstOrDefault();
                    try
                    {
                        comision.Cod_Vendedor = codvendedor;
                        comision.NombreVendedor = vendedor.Nombre;
                        db.SaveChanges();
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        m_errorLabel.Text = ex.InnerException.Message;
                        m_errorLabel.ForeColor = Color.Red;
                        transaction.Rollback();
                        return;
                    }
                    m_errorLabel.Text = "Comision reasignada a " + vendedor.Nombre;
                    m_errorLabel.ForeColor = Color.Green;
                }
            }
        }

        private void ReasignarComision_Load(object sender, EventArgs e)
        {
            ActualizarVendedores();
        }
    }
}
