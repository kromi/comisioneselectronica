﻿namespace ComisionesElectronica.Windows
{
    partial class AsignarComision
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_bienvenidaLabel = new System.Windows.Forms.Label();
            this.m_sucursalLabel = new System.Windows.Forms.Label();
            this.m_facturasList = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.m_vendedoresList = new System.Windows.Forms.ComboBox();
            this.m_guardarButton = new System.Windows.Forms.Button();
            this.m_cerrarSesionLink = new System.Windows.Forms.LinkLabel();
            this.m_reasignarComisionLink = new System.Windows.Forms.LinkLabel();
            this.m_actualizarFacturasButton = new System.Windows.Forms.Button();
            this.m_errorLabel = new System.Windows.Forms.Label();
            this.m_vendedorText = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // m_bienvenidaLabel
            // 
            this.m_bienvenidaLabel.AutoSize = true;
            this.m_bienvenidaLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_bienvenidaLabel.Location = new System.Drawing.Point(12, 9);
            this.m_bienvenidaLabel.Name = "m_bienvenidaLabel";
            this.m_bienvenidaLabel.Size = new System.Drawing.Size(106, 20);
            this.m_bienvenidaLabel.TabIndex = 0;
            this.m_bienvenidaLabel.Text = "Bienvenido(a)";
            // 
            // m_sucursalLabel
            // 
            this.m_sucursalLabel.AutoSize = true;
            this.m_sucursalLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_sucursalLabel.Location = new System.Drawing.Point(12, 33);
            this.m_sucursalLabel.Name = "m_sucursalLabel";
            this.m_sucursalLabel.Size = new System.Drawing.Size(0, 20);
            this.m_sucursalLabel.TabIndex = 1;
            // 
            // m_facturasList
            // 
            this.m_facturasList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_facturasList.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_facturasList.FormattingEnabled = true;
            this.m_facturasList.Location = new System.Drawing.Point(130, 72);
            this.m_facturasList.Name = "m_facturasList";
            this.m_facturasList.Size = new System.Drawing.Size(263, 28);
            this.m_facturasList.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(29, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "Factura";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(29, 125);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "Vendedor";
            // 
            // m_vendedoresList
            // 
            this.m_vendedoresList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_vendedoresList.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_vendedoresList.FormattingEnabled = true;
            this.m_vendedoresList.Location = new System.Drawing.Point(130, 122);
            this.m_vendedoresList.Name = "m_vendedoresList";
            this.m_vendedoresList.Size = new System.Drawing.Size(263, 28);
            this.m_vendedoresList.TabIndex = 4;
            this.m_vendedoresList.Visible = false;
            // 
            // m_guardarButton
            // 
            this.m_guardarButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.m_guardarButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_guardarButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.m_guardarButton.Location = new System.Drawing.Point(130, 175);
            this.m_guardarButton.Name = "m_guardarButton";
            this.m_guardarButton.Size = new System.Drawing.Size(202, 74);
            this.m_guardarButton.TabIndex = 6;
            this.m_guardarButton.Text = "GUARDAR";
            this.m_guardarButton.UseVisualStyleBackColor = false;
            this.m_guardarButton.Click += new System.EventHandler(this.m_guardarButton_Click);
            // 
            // m_cerrarSesionLink
            // 
            this.m_cerrarSesionLink.AutoSize = true;
            this.m_cerrarSesionLink.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_cerrarSesionLink.Location = new System.Drawing.Point(409, 9);
            this.m_cerrarSesionLink.Name = "m_cerrarSesionLink";
            this.m_cerrarSesionLink.Size = new System.Drawing.Size(119, 20);
            this.m_cerrarSesionLink.TabIndex = 7;
            this.m_cerrarSesionLink.TabStop = true;
            this.m_cerrarSesionLink.Text = "Cerrar Sesión";
            this.m_cerrarSesionLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.m_cerrarSesionLink_LinkClicked);
            // 
            // m_reasignarComisionLink
            // 
            this.m_reasignarComisionLink.AutoSize = true;
            this.m_reasignarComisionLink.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_reasignarComisionLink.Location = new System.Drawing.Point(16, 292);
            this.m_reasignarComisionLink.Name = "m_reasignarComisionLink";
            this.m_reasignarComisionLink.Size = new System.Drawing.Size(117, 13);
            this.m_reasignarComisionLink.TabIndex = 8;
            this.m_reasignarComisionLink.TabStop = true;
            this.m_reasignarComisionLink.Text = "Reasignar comisión";
            this.m_reasignarComisionLink.Visible = false;
            this.m_reasignarComisionLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.m_reasignarComisionLink_LinkClicked);
            // 
            // m_actualizarFacturasButton
            // 
            this.m_actualizarFacturasButton.BackColor = System.Drawing.Color.Teal;
            this.m_actualizarFacturasButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_actualizarFacturasButton.ForeColor = System.Drawing.Color.White;
            this.m_actualizarFacturasButton.Location = new System.Drawing.Point(413, 73);
            this.m_actualizarFacturasButton.Name = "m_actualizarFacturasButton";
            this.m_actualizarFacturasButton.Size = new System.Drawing.Size(110, 76);
            this.m_actualizarFacturasButton.TabIndex = 9;
            this.m_actualizarFacturasButton.Text = "Actualizar Facturas";
            this.m_actualizarFacturasButton.UseVisualStyleBackColor = false;
            this.m_actualizarFacturasButton.Click += new System.EventHandler(this.m_actualizarFacturasButton_Click);
            // 
            // m_errorLabel
            // 
            this.m_errorLabel.AutoSize = true;
            this.m_errorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_errorLabel.ForeColor = System.Drawing.Color.Red;
            this.m_errorLabel.Location = new System.Drawing.Point(133, 252);
            this.m_errorLabel.Name = "m_errorLabel";
            this.m_errorLabel.Size = new System.Drawing.Size(0, 20);
            this.m_errorLabel.TabIndex = 10;
            // 
            // m_vendedorText
            // 
            this.m_vendedorText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_vendedorText.Location = new System.Drawing.Point(130, 125);
            this.m_vendedorText.Name = "m_vendedorText";
            this.m_vendedorText.Size = new System.Drawing.Size(263, 26);
            this.m_vendedorText.TabIndex = 11;
            this.m_vendedorText.Leave += new System.EventHandler(this.m_vendedorText_Leave);
            // 
            // AsignarComision
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(534, 309);
            this.ControlBox = false;
            this.Controls.Add(this.m_vendedorText);
            this.Controls.Add(this.m_errorLabel);
            this.Controls.Add(this.m_actualizarFacturasButton);
            this.Controls.Add(this.m_reasignarComisionLink);
            this.Controls.Add(this.m_cerrarSesionLink);
            this.Controls.Add(this.m_guardarButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.m_vendedoresList);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_facturasList);
            this.Controls.Add(this.m_sucursalLabel);
            this.Controls.Add(this.m_bienvenidaLabel);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AsignarComision";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Comisión Electrónica - Asignar Comisión";
            this.Load += new System.EventHandler(this.AsignarComision_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label m_bienvenidaLabel;
        private System.Windows.Forms.Label m_sucursalLabel;
        private System.Windows.Forms.ComboBox m_facturasList;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox m_vendedoresList;
        private System.Windows.Forms.Button m_guardarButton;
        private System.Windows.Forms.LinkLabel m_cerrarSesionLink;
        private System.Windows.Forms.LinkLabel m_reasignarComisionLink;
        private System.Windows.Forms.Button m_actualizarFacturasButton;
        private System.Windows.Forms.Label m_errorLabel;
        private System.Windows.Forms.TextBox m_vendedorText;
    }
}