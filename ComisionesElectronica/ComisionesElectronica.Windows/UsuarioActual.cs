﻿using ComisionesElectronica.Data.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ComisionesElectronica.Windows
{
    public static class UsuarioActual
    {
        public static Usuario Usuario { get; set; }
        public static Sucursal Sucursal { get; set; }
        
    }
}
