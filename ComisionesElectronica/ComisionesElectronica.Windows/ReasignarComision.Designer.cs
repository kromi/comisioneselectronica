﻿namespace ComisionesElectronica.Windows
{
    partial class ReasignarComision
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_guardarButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.m_vendedoresList = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.m_errorLabel = new System.Windows.Forms.Label();
            this.m_facturaText = new System.Windows.Forms.TextBox();
            this.m_regresarLink = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // m_guardarButton
            // 
            this.m_guardarButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.m_guardarButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_guardarButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.m_guardarButton.Location = new System.Drawing.Point(235, 152);
            this.m_guardarButton.Name = "m_guardarButton";
            this.m_guardarButton.Size = new System.Drawing.Size(202, 74);
            this.m_guardarButton.TabIndex = 11;
            this.m_guardarButton.Text = "GUARDAR";
            this.m_guardarButton.UseVisualStyleBackColor = false;
            this.m_guardarButton.Click += new System.EventHandler(this.m_guardarButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(23, 97);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 20);
            this.label2.TabIndex = 10;
            this.label2.Text = "Vendedor";
            // 
            // m_vendedoresList
            // 
            this.m_vendedoresList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_vendedoresList.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_vendedoresList.FormattingEnabled = true;
            this.m_vendedoresList.Location = new System.Drawing.Point(124, 94);
            this.m_vendedoresList.Name = "m_vendedoresList";
            this.m_vendedoresList.Size = new System.Drawing.Size(515, 28);
            this.m_vendedoresList.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(23, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 20);
            this.label1.TabIndex = 8;
            this.label1.Text = "Factura";
            // 
            // m_errorLabel
            // 
            this.m_errorLabel.AutoSize = true;
            this.m_errorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_errorLabel.ForeColor = System.Drawing.Color.Red;
            this.m_errorLabel.Location = new System.Drawing.Point(23, 264);
            this.m_errorLabel.Name = "m_errorLabel";
            this.m_errorLabel.Size = new System.Drawing.Size(0, 20);
            this.m_errorLabel.TabIndex = 12;
            // 
            // m_facturaText
            // 
            this.m_facturaText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_facturaText.Location = new System.Drawing.Point(124, 46);
            this.m_facturaText.Name = "m_facturaText";
            this.m_facturaText.Size = new System.Drawing.Size(515, 26);
            this.m_facturaText.TabIndex = 13;
            this.m_facturaText.Leave += new System.EventHandler(this.m_facturaText_Leave);
            // 
            // m_regresarLink
            // 
            this.m_regresarLink.AutoSize = true;
            this.m_regresarLink.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_regresarLink.Location = new System.Drawing.Point(556, 264);
            this.m_regresarLink.Name = "m_regresarLink";
            this.m_regresarLink.Size = new System.Drawing.Size(83, 20);
            this.m_regresarLink.TabIndex = 14;
            this.m_regresarLink.TabStop = true;
            this.m_regresarLink.Text = "Regresar";
            this.m_regresarLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.m_regresarLink_LinkClicked);
            // 
            // ReasignarComision
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Teal;
            this.ClientSize = new System.Drawing.Size(669, 313);
            this.Controls.Add(this.m_regresarLink);
            this.Controls.Add(this.m_facturaText);
            this.Controls.Add(this.m_errorLabel);
            this.Controls.Add(this.m_guardarButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.m_vendedoresList);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "ReasignarComision";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Comisiones Electrónica - Reasignar Comision";
            this.Load += new System.EventHandler(this.ReasignarComision_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button m_guardarButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox m_vendedoresList;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label m_errorLabel;
        private System.Windows.Forms.TextBox m_facturaText;
        private System.Windows.Forms.LinkLabel m_regresarLink;
    }
}