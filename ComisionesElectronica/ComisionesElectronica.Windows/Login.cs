﻿using ComisionesElectronica.Data.Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ComisionesElectronica.Windows
{
    public partial class Login : Form
    {
        public Login()
        {
            db = new ComisionElectronicaContext();
            InitializeComponent();
            m_sucursalList.DataSource = db.Sucursales.ToList();
            m_sucursalList.DisplayMember = "C_descripcion";
            m_sucursalList.ValueMember = "C_codigo";
            
        }
        private ComisionElectronicaContext db;
        
        private void _enviarButton_Click(object sender, EventArgs e)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                if (m_sucursalList.SelectedValue != null)
                {
                    string sucursalSeleccionada = m_sucursalList.SelectedValue.ToString();
                    Usuario usuario = new Usuario();
                    try
                    {
                        usuario = db.Usuarios.Where(x => x.Login == m_nombreUsuarioText.Text && x.Password == m_claveText.Text).FirstOrDefault();
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        _errorLabel.Text = ex.Message;
                        transaction.Rollback();
                    }
                    if (usuario == null)
                    {
                        _errorLabel.Text = "Nombre de Usuario y//o Contraseña invalidos";
                        _errorLabel.ForeColor = Color.Red;
                        return;
                    }
                    UsuarioActual.Usuario = usuario;
                    UsuarioActual.Sucursal = db.Sucursales.Where(s => s.C_codigo == sucursalSeleccionada).FirstOrDefault();
                    AsignarComision asignar = new AsignarComision();
                    asignar.ShowDialog();
                }
                else
                {
                    _errorLabel.Text = "Debe seleccionar una sucursal";
                    _errorLabel.ForeColor = Color.Red;
                }

            }
        }

        private void m_claveText_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                m_enviarButton.PerformClick();
            }
        }

        private void m_reestablecerButton_Click(object sender, EventArgs e)
        {
            m_nombreUsuarioText.Text = "";
            m_claveText.Text = "";
        }
    }
}
