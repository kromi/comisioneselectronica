﻿using ComisionesElectronica.Data.Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ComisionesElectronica.Windows
{
    public partial class AsignarComision : Form
    {
        public AsignarComision()
        {
            InitializeComponent();
            db = new ComisionElectronicaContext();
        }

        private ComisionElectronicaContext db;
        private void AsignarComision_Load(object sender, EventArgs e)
        {
            if (UsuarioActual.Usuario != null)
            {
                m_bienvenidaLabel.Text = "Bienvenido(a) " + UsuarioActual.Usuario.Descripcion;
                m_sucursalLabel.Text = "Sucursal " + UsuarioActual.Sucursal.C_descripcion;

                ActualizarListados();

                if (UsuarioActual.Usuario.Login == "CCANDELORI")
                {
                    m_reasignarComisionLink.Visible = true;
                }
            }
        }

        private void ActualizarListados()
        {
            var facturasConComision = db.ComisionesElectronica.Select(c => c.NumeroFactura);
            List<Facturas> facturas = new List<Facturas>();
            var facturasSinComision = db.Facturas.Where(f => f.Sucursal == UsuarioActual.Sucursal.C_codigo).Distinct().Select(x => x.NumeroFactura).Except(facturasConComision).ToList();
            foreach (var factura in facturasSinComision)
            {
                facturas.Add(new Facturas() { NumeroFactura = factura });
            }
            m_facturasList.DataSource = facturas;
            m_facturasList.DisplayMember = "NumeroFactura";
            m_facturasList.ValueMember = "NumeroFactura";

            m_vendedoresList.DataSource = db.Vendedores.ToList();
            m_vendedoresList.DisplayMember = "Nombre";
            m_vendedoresList.ValueMember = "Codigo";
        }

        private void m_cerrarSesionLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (MessageBox.Show("Está seguro que desea cerrar sesión?","Alerta",MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Application.Restart();
            }
        }

        private void m_guardarButton_Click(object sender, EventArgs e)
        {
            if (m_facturasList.SelectedValue != null && (m_vendedoresList.SelectedValue != null || m_vendedorText.Text != ""))
            {
                string codVendedor = "";
                if (db.ComisionesElectronica.Any(f => f.NumeroFactura == m_facturasList.SelectedValue.ToString()))
                {
                    m_errorLabel.Text = "Factura ya asignada, por favor actualice el listado de facturas";
                    m_errorLabel.ForeColor = Color.Red;
                    return;
                }
                var factura = (from f in db.Facturas
                               where f.NumeroFactura == m_facturasList.SelectedValue.ToString() && f.Concepto == "VEN"
                               group f by new { f.Cod_Principal, f.Concepto, f.Fecha, f.NumeroFactura, f.Sucursal } into g
                               select new
                               {
                                   Cod_Principal = g.Key.Cod_Principal,
                                   C_Concepto = g.Key.Concepto,
                                   FechaFactura = g.Key.Fecha,
                                   NumeroFactura = g.Key.NumeroFactura,
                                   Sucursal = g.Key.Sucursal,
                                   SubTotal = g.Sum(n => n.SubTotal)

                               }).FirstOrDefault();
                PorcentajeComision porcentaje = db.PorcentajesComision.Where(p => p.SettingKey == "ComisionElectronica").FirstOrDefault();
                codVendedor = m_vendedoresList.SelectedValue.ToString();
                Vendedor vendedor = db.Vendedores.Where(v => v.Codigo == codVendedor).FirstOrDefault();
                ComisionElectronica comision = new ComisionElectronica();
                comision.NumeroFactura = factura.NumeroFactura;
                comision.Cod_Vendedor = codVendedor;
                comision.NombreVendedor = vendedor.Nombre;
                comision.Porcentaje_Comision = Convert.ToDecimal(porcentaje.Value);
                comision.SubTotal = factura.SubTotal;
                comision.Sucursal = UsuarioActual.Sucursal.C_codigo;
                comision.Cod_Producto = factura.Cod_Principal;
                comision.Concepto = factura.C_Concepto;
                comision.Fecha = factura.FechaFactura;
                comision.CreadoPor = UsuarioActual.Usuario.Login;
                comision.CreadoEl = DateTime.Now;
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        db.ComisionesElectronica.Add(comision);
                        db.SaveChanges();
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        m_errorLabel.Text = ex.InnerException.Message;
                        m_errorLabel.ForeColor = Color.Red;
                        transaction.Rollback();

                    }
                    m_errorLabel.Text = "Última Factura asignada a " + vendedor.Nombre;
                    m_errorLabel.ForeColor = Color.Green;
                }
            }
            else
            {
                m_errorLabel.Text = "Debe Seleccionar una Factura y un Vendedor, gracias";
            }
        }
    

        private void ActualizaFacturas()
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    db.Database.ExecuteSqlCommand("EXEC cap_ActualizarFacturasElectronica");
                    transaction.Commit();
                    ActualizarListados();
                }
                catch (Exception ex)
                {
                    m_errorLabel.Text = ex.InnerException.Message;
                    transaction.Rollback();
                }

            }
        }

        private void m_actualizarFacturasButton_Click(object sender, EventArgs e)
        {
            ActualizaFacturas();
        }

        private void m_reasignarComisionLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ReasignarComision reasignar = new ReasignarComision();
            reasignar.ShowDialog();
        }

        private void m_vendedorText_Leave(object sender, EventArgs e)
        {
            if (m_vendedorText.Text != "")
            {
                if (db.Vendedores.Any(x => x.Codigo == m_vendedorText.Text))
                {
                    m_errorLabel.Text = "Vendedor Existente: " + db.Vendedores.Where(x => x.Codigo == m_vendedorText.Text).FirstOrDefault().Nombre;
                    m_errorLabel.ForeColor = Color.Green;
                }
                else
                {
                    m_errorLabel.Text = "Vendedor no registrado";
                    m_errorLabel.ForeColor = Color.Red;
                }
            }
        }
    }
}
