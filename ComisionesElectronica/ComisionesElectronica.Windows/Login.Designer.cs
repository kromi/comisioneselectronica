﻿namespace ComisionesElectronica.Windows
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_nombreUsuarioText = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.m_claveText = new System.Windows.Forms.TextBox();
            this.m_enviarButton = new System.Windows.Forms.Button();
            this.m_reestablecerButton = new System.Windows.Forms.Button();
            this._errorLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.m_sucursalList = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // m_nombreUsuarioText
            // 
            this.m_nombreUsuarioText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_nombreUsuarioText.Location = new System.Drawing.Point(132, 27);
            this.m_nombreUsuarioText.Name = "m_nombreUsuarioText";
            this.m_nombreUsuarioText.Size = new System.Drawing.Size(226, 26);
            this.m_nombreUsuarioText.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(34, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Usuario:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(34, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Contraseña:";
            // 
            // m_claveText
            // 
            this.m_claveText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_claveText.Location = new System.Drawing.Point(132, 61);
            this.m_claveText.Name = "m_claveText";
            this.m_claveText.PasswordChar = '*';
            this.m_claveText.Size = new System.Drawing.Size(226, 26);
            this.m_claveText.TabIndex = 1;
            this.m_claveText.KeyDown += new System.Windows.Forms.KeyEventHandler(this.m_claveText_KeyDown);
            // 
            // m_enviarButton
            // 
            this.m_enviarButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.m_enviarButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_enviarButton.Location = new System.Drawing.Point(38, 140);
            this.m_enviarButton.Name = "m_enviarButton";
            this.m_enviarButton.Size = new System.Drawing.Size(165, 36);
            this.m_enviarButton.TabIndex = 3;
            this.m_enviarButton.Text = "Enviar";
            this.m_enviarButton.UseVisualStyleBackColor = false;
            this.m_enviarButton.Click += new System.EventHandler(this._enviarButton_Click);
            // 
            // m_reestablecerButton
            // 
            this.m_reestablecerButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.m_reestablecerButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_reestablecerButton.Location = new System.Drawing.Point(209, 140);
            this.m_reestablecerButton.Name = "m_reestablecerButton";
            this.m_reestablecerButton.Size = new System.Drawing.Size(149, 36);
            this.m_reestablecerButton.TabIndex = 4;
            this.m_reestablecerButton.Text = "Reestablecer";
            this.m_reestablecerButton.UseVisualStyleBackColor = false;
            this.m_reestablecerButton.Click += new System.EventHandler(this.m_reestablecerButton_Click);
            // 
            // _errorLabel
            // 
            this._errorLabel.AutoSize = true;
            this._errorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._errorLabel.ForeColor = System.Drawing.Color.Red;
            this._errorLabel.Location = new System.Drawing.Point(38, 179);
            this._errorLabel.Name = "_errorLabel";
            this._errorLabel.Size = new System.Drawing.Size(0, 20);
            this._errorLabel.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(33, 101);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 20);
            this.label3.TabIndex = 8;
            this.label3.Text = "Sucursal:";
            // 
            // m_sucursalList
            // 
            this.m_sucursalList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_sucursalList.FormattingEnabled = true;
            this.m_sucursalList.Location = new System.Drawing.Point(132, 100);
            this.m_sucursalList.Name = "m_sucursalList";
            this.m_sucursalList.Size = new System.Drawing.Size(226, 21);
            this.m_sucursalList.TabIndex = 2;
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(391, 230);
            this.Controls.Add(this.m_sucursalList);
            this.Controls.Add(this.label3);
            this.Controls.Add(this._errorLabel);
            this.Controls.Add(this.m_reestablecerButton);
            this.Controls.Add(this.m_enviarButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.m_claveText);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_nombreUsuarioText);
            this.MaximizeBox = false;
            this.Name = "Login";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Comisiones Electrónica - Inicio de Sesión";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox m_nombreUsuarioText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox m_claveText;
        private System.Windows.Forms.Button m_enviarButton;
        private System.Windows.Forms.Button m_reestablecerButton;
        private System.Windows.Forms.Label _errorLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox m_sucursalList;
    }
}

