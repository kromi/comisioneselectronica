//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ComisionesElectronica.MVC.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class vw_Vendedores
    {
        public Nullable<int> Row { get; set; }
        public string Codigo { get; set; }
        public string Nombre { get; set; }
        public string CU_VENDEDOR_COD { get; set; }
        public string CU_VENDEDOR_DES { get; set; }
        public bool BU_ACTIVO { get; set; }
        public string cs_RELACION { get; set; }
        public string cs_LOCALIDAD { get; set; }
        public string cs_LOGIN { get; set; }
        public string cs_PASSWORD { get; set; }
        public System.DateTime cs_INGRESO { get; set; }
        public System.DateTime cs_MODIFICADO { get; set; }
        public string cs_GRUPO_COMISION { get; set; }
        public bool bu_cualquier_cuenta { get; set; }
        public string cs_tipo { get; set; }
        public string CU_RIF { get; set; }
        public string C_COMISIONES { get; set; }
    }
}
