﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ComisionesElectronica.MVC.Models
{

    public class ApplicationUserStore : UserStore<IdentityUser>, IUserStore<IdentityUser>, IDisposable
    {
        public ApplicationUserStore(ApplicationDbContext context) : base(context)
        {

        }

        public new void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}