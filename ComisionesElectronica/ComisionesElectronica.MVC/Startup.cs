﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ComisionesElectronica.MVC.Startup))]
namespace ComisionesElectronica.MVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
